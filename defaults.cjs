function checkIfPresentInArray(arr, key){

    for (let index = 0; index < arr.length; index++){
        if (arr[index] === key) {
            return true;
        }
    }

    return false;
}

function defaults(obj, defaultProps){
    if(typeof obj != 'object'){
        return [];
    }
    if(typeof obj != 'object'){
        return [];
    }

    let allkeyObject = [];

    for(let key in obj){
        allkeyObject.push(key);
    }

    let allkeyDefaultProps = [];
    for(let key in defaultProps){
        allkeyDefaultProps.push(key);
    }

    for (let columnIndex = 0; columnIndex < allkeyDefaultProps.length; columnIndex++) {

        const key = allkeyDefaultProps[columnIndex];

        if (!checkIfPresentInArray(allkeyObject, key)) {

            obj[key] = defaultProps[key];

        }
    }

    return obj;
}

module.exports = defaults;