function invert(obj) {
    if(typeof obj != 'object'){
        return [];
    }

    const invertResult = {};
    let allKey = [];
    for(let key in obj){
        allKey.push(key);
    }

    for (let columnIndex = 0; columnIndex < allKey.length; columnIndex++){
        const key = allKey[columnIndex];
        const val = obj[allKey[columnIndex]];
        invertResult[val] = key;
    }

    return invertResult;
}

module.exports = invert;