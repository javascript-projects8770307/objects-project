function keys(obj){  

    if(typeof obj != 'object'){
        return [];
    }

    let keyResult = [];

    for (let key in obj){
        keyResult.push(key);
    }

    return keyResult;
}
module.exports = keys;