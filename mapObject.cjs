function mapObject(obj, cb){

    if(typeof obj != 'object'){
        return [];
    }
    if(typeof cb != 'function'){
        return obj;
    }

    const allKey = [];
    for(let key in obj){
        allKey.push(key);
    }

    for (let columnIndex = 0; columnIndex < allKey.length; columnIndex++) {
        const key = allKey[columnIndex];
        const val = obj[key];
        obj[key] = cb(val, key);
    }

    return obj;
}

module.exports = mapObject;

