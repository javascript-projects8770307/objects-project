function pairs(obj) {
    if(typeof obj != 'object'){
        return [];
    }
    const resultPairs = [];
    
    let allKey = [];
    for(var key in obj){
        allKey.push(key);
    }

    for (let columnIndex = 0; columnIndex < allKey.length; columnIndex++){
        resultPairs.push([allKey[columnIndex], obj[allKey[columnIndex]]]);
    }

    return resultPairs;
}

module.exports = pairs;