const mapObject = require('../mapObject.cjs');
const obj = { age1: 20, age2: 25, age3: 40 };

const result = mapObject(obj, (val, key)=> {
    return val + 10;
});
console.log(JSON.stringify(result));
