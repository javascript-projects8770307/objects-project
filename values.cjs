function values(obj){

    if(typeof obj != 'object'){
        return [];
    }

    let objectKeys = [];

    for(let key in obj){
        objectKeys.push(key);
    }

    let resultValues = [];

    for(let columnIndex = 0; columnIndex < objectKeys.length; columnIndex++){

        if(typeof obj[objectKeys[columnIndex]]=='function'){
            continue;
        }
        resultValues.push(obj[objectKeys[columnIndex]]);
    }
    
    return resultValues;   
}

module.exports = values;